# Requirements

- GitLab (inc. hosted gitlab.com)
- A server controlled by [Docker Machine](https://docs.docker.com/machine/)
- [nginx-proxy](https://github.com/jwilder/nginx-proxy) deployed on server in `proxy` network.

## nginx-proxy configuraton

- Example config is in [one-server-proxy-static-letsencrypt](../one-server-proxy-static-letsencrypt)
- You have to create `proxy` network manually:
   ```
   docker network create proxy
   docker network connect proxy oneserverproxystaticletsencrypt_proxy_1  # Use your proxy container name
   ```

# How to configure GitLab and Docker Machine

1. `docker-machine create …` ([available options for different providers](https://docs.docker.com/machine/drivers/))

2. In GitLab project open CI settings and configure this secret variables:

    | Variable name              | Description                  | Example value
    | ---------------------------|------------------------------|--------------------------------------------
    | REVIEW_DOMAIN              | domain for [review apps](https://docs.gitlab.com/ee/ci/review_apps/) | ci.example.com
    | DOCKER_MACHINE_NAME        | machine name (default:       | default - default value
    | DOCKER_MACHINE_URL         | ip address of target machine | 44.32.5.145
    | DOCKER_MACHINE_PORT        | target machine docker port   | 2376 - default value
    | DOCKER_MACHINE_CA_CERT     | contents of ca.pem           | `-----BEGIN CERTIFICATE-----`
    | DOCKER_MACHINE_CA_CERT_KEY | contents of ca-key.pem       | `-----BEGIN RSA PRIVATE KEY-----`
    | DOCKER_MACHINE_CERT        | contents of cert.pem         | `-----BEGIN CERTIFICATE-----`
    | DOCKER_MACHINE_CERT_KEY    | contents of key.pem          | `-----BEGIN RSA PRIVATE KEY-----`

3. Put init database to the server where CI wil be deployed (or better to some volume on shared storage if using multiple nodes). If you do not need to initialize database by default schema and data, just remove volume in docker-compose.ci.yml.
