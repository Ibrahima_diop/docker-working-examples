# Working examples of my Docker stacks

## [drupal-composer-gitlab-ci-rancher](drupal-composer-gitlab-ci-rancher)

- Drupal 8 from [Composer template](https://github.com/drupal-composer/drupal-project) with MariaDB, Memcached
- Uses my [base image for Drupal with Composer](https://github.com/iBobik/drupal-composer-docker)
- CI and production deployment to Rancher cluster (separate production and development environment)

## [drupal-composer-memcached](drupal-composer-memcached)

- Drupal 8 from [Composer template](https://github.com/drupal-composer/drupal-project) with MariaDB, Memcached
- Uses my [base image for Drupal with Composer](https://github.com/iBobik/drupal-composer-docker)
- Nice README for team members
- Just for local development

## gitlab-ci-with-*

- Drupal 7 with PHP 5.6, Drush and MySQL 5
- Automatic import of DB with schema and example data
- Init script waiting for available DB and then triggers other scripts like DB migration
- Conditional Drupal configuration by environment varibales
- Compose files for local development, CI and production with overriding
- GitLab CI configuration builds web image, deploys [review apps](https://docs.gitlab.com/ee/ci/review_apps/) (all branches) and manually production (master only)

### [gitlab-ci-with-docker-machine](gitlab-ci-with-docker-machine)

- Deploy target for CI is Docker Machine (one permanent server accessible by SSH)

### [gitlab-ci-with-rancher](gitlab-ci-with-rancher)

- Deploy target is Rancher cluster (separate production and development environment)

## [one-server-proxy-static-letsencrypt](one-server-proxy-static-letsencrypt)

- Proxy for every server with multiple websites (because only one container can expose port 80 and 443)
- Uses [nginx-proxy](https://github.com/jwilder/nginx-proxy)
- Termination of HTTPS with automatic Let's Encrypt certs generation by [letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)
- Static hosting for simple website deployed [securely](https://github.com/iBobik/gitlab-ci-deployer) by GitLab CI (aka GitHub Pages or GitLab Pages)

# .bash_profile tweeks

Useful functions and prompt customizations what helps me in my everyday work.

- Customize bash prompt to print current GIT branch and Docker Machine name.
- `dmenv`: Set env variables for Docker Machine
- `dstats`: Dockers stats with container names
- `dsh`: Open shell into app or php service of current Docker Compose project
- `uli`: Get login link for Drupal superuser without entering container

![how it works](https://user-images.githubusercontent.com/614232/34212178-c9bb110e-e59b-11e7-82c6-d9c875cc221c.gif)

[see my .bash_profile file](https://gist.github.com/iBobik/ba6eb36cb959b7ce88f50298069ec39f)
